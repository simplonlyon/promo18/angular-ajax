import { Component, OnInit } from '@angular/core';
import { Operation } from '../entities';
import { OperationService } from '../operation.service';

@Component({
  selector: 'app-search',
  templateUrl: './search.component.html',
  styleUrls: ['./search.component.css']
})
export class SearchComponent implements OnInit {

  // Attribut qui va faire le lien avec le champ de formulaire
  labelFilter?:string;
  // Liste d'opérations pour le retour de la requête
  list?:Operation[] = [];
  // Booléen pour gérer l'affichage de la liste de résultats
  displayResultList:boolean = false;


  constructor(private opService:OperationService) {
    this.labelFilter = "";
   }

  ngOnInit(): void {
  }

  searchByLabel() {
    if (this.labelFilter?.length == 0) {
      alert("Merci de saisir une valeur !");
      document.getElementById('labelFilter')?.focus();
    } else {
      this.opService.search(this.labelFilter!).subscribe(
        data => this.list = data   
      );
      this.displayResultList = true;
    }
  }
}
