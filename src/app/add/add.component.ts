import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Operation } from '../entities';
import { OperationService } from '../operation.service';

@Component({
  selector: 'app-add',
  templateUrl: './add.component.html',
  styleUrls: ['./add.component.css']
})
export class AddComponent implements OnInit {

  operation:Operation = {
    amount: 0,
    label: ''
  };
  constructor(private opService:OperationService, private router:Router) { }

  ngOnInit(): void {
  }

  addOperation() {
    this.opService.add(this.operation).subscribe(() => {
      this.router.navigate(['/']);
    }); //On oublie pas le subscribe, sinon la requête ne sera pas lancée
  }
}
