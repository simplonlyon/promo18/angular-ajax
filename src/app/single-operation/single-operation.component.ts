import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Operation } from '../entities';
import { OperationService } from '../operation.service';

@Component({
  selector: 'app-single-operation',
  templateUrl: './single-operation.component.html',
  styleUrls: ['./single-operation.component.css']
})
export class SingleOperationComponent implements OnInit {

  item?:Operation;
  routeId?:string;
  arg?:string;

  constructor(private opService:OperationService, private route:ActivatedRoute) { }

  ngOnInit(): void {
    // Paramètre de la requête, ce qu'il y a APRÈS le ?
    // operation/:id/?arg=bonjour
    this.route.queryParams.subscribe(params => {
      console.log(params);
      this.arg = params['arg'];
    });
    // Fragment variable de l'URL, AVANT le ?
    this.route.params.subscribe(param => {
      this.routeId = param['id'];
    });
    this.opService.getById(Number(this.routeId)).subscribe(data => this.item = data);
  }

}
