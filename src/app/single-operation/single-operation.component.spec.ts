import { ComponentFixture, TestBed } from '@angular/core/testing';

import { SingleOperationComponent } from './single-operation.component';

describe('SingleOperationComponent', () => {
  let component: SingleOperationComponent;
  let fixture: ComponentFixture<SingleOperationComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ SingleOperationComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(SingleOperationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
