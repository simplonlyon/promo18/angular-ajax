import { Component, OnInit } from '@angular/core';
import { Operation } from '../entities';
import { OperationService } from '../operation.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  list:Operation[] = []
  single?:Operation;
  displayEditForm:boolean = false;
  modifiedOp:Operation = {label: "", amount: 0};
  constructor(private opService:OperationService) { }

  ngOnInit(): void {
    this.opService.getAll().subscribe(data => this.list = data);
  }

  fetchOne() {
    this.opService.getById(2).subscribe(data => this.single = data);
  }

  delete(id:number) {
    this.opService.delete(id).subscribe();
    this.removeFromList(id);
  }

  removeFromList(id: number) {
    this.list.forEach((operation, operation_index) => {
      if(operation.id == id) {
        this.list.splice(operation_index, 1);
      }
    });
  }

  fetchAll(){
    this.opService.getAll().subscribe(data => this.list = data);
  }

  showEditForm(operation:Operation) {
    this.displayEditForm = true;
    // Impossible, le = copie juste la référence, il ne fait pas une copie
    // this.modifiedOp = operation;
    this.modifiedOp = Object.assign({},operation);
  }

  update(operation:Operation) {
    // Envoyer au serveur
    this.opService.put(operation).subscribe();
    // Mettre à jour la liste en HTML
    this.updateOperationInList(operation);
    // Fermer le formulaire
    this.displayEditForm = false;
  }

  updateOperationInList(operation:Operation) {
    this.list.forEach((op_list) => {
      if(op_list.id == operation.id) {
        op_list.amount = operation.amount;
        op_list.date = operation.date;
        op_list.label = operation.label;
      }
    });
  }

}
