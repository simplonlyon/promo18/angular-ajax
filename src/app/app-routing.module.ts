import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AddComponent } from './add/add.component';
import { HomeComponent } from './home/home.component';
import { SearchComponent } from './search/search.component';
import { SingleOperationComponent } from './single-operation/single-operation.component';

const routes: Routes = [
  {path: '', component: HomeComponent}, //définir la page d'accueil de l'appli
  {path: 'search', component: SearchComponent}, //page de recherche
  {path:'operation/:id', component:SingleOperationComponent}, // route paramétrée pour afficher UNE opération
  {path: 'add-operation', component: AddComponent} //http://localhost:4200/add-operation
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
