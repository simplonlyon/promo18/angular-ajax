export interface Operation {
    id?:number;
    label:string;
    date?:string;
    amount:number;
}